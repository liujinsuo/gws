package main

import (
	"fmt"
	"gitee.com/liujinsuo/gws"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"time"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	wsServer := gws.NewWebsocketServer()

	wsServer.Handler = func(conn *gws.Conn) {

		defer func() {

			if err := conn.Close(); err != nil {
				log.Println("关闭失败", err) //重复关闭会报错
			}
		}()

		conn.Request().ParseForm()
		group := conn.Request().URL.Query().Get("group")

		RoomName := "test1"
		//conn.Rooms().Join(RoomName, conn) //加入房间
		conn.Rooms().Join(group, conn) //加入房间
		conn.Rooms().Len(RoomName)     //指定房间的连接个数

		log.Println("收到新连接")

		//reader, err := conn.NewFrameReader()
		//if err != nil {
		//	fmt.Println(reader, err)
		//}
		//frame, err := conn.HandleFrame(reader)
		//fmt.Println(frame, err)

		go func() {
			for {
				time.Sleep(time.Millisecond * 1000)
				if conn.IsClose() == true {
					log.Println("连接已经关闭")
					break
				}
				//发送ping消息
				if err := conn.Write(websocket.PingMessage, []byte("ping")); err != nil {
					log.Println(err)
				} else {
					log.Println("发ping成功")
				}
			}
		}()
		conn.SetPongHandler(func(appData string) error {
			log.Println("处理Pong消息")
			return nil
		})

		conn.SetCloseHandler(func(code int, text string) error {
			log.Println("处理close消息")
			return nil
		})
		conn.SetPingHandler(func(appData string) error {
			log.Println("处理ping消息")
			return nil
		})

		conn.Rooms().Copy(RoomName, group)
		conn.Set("test", 10)
		a := conn.GetInt64("test")
		log.Println(a)

		for {

			//r, err := conn.NewFrameReader()
			//log.Println("读取帧数据", r.PayloadType(), err)

			reply := ""
			messageType, p, err := conn.ReadMessage()
			if e, ok := err.(*websocket.CloseError); ok == true { //io.EOF一般是客户端关闭
				log.Println("ws", "连接断开", e)
				break
			} else if err != nil { //客户端不关闭直接断网会走到这
				log.Println("ws", "读取消息失败", err)
				break
			} else {
				reply = string(p)
				log.Println("ws", conn.ID(), "收到消息", reply)
				conn.Write(messageType, []byte("收到消息:"+reply)) //Write写入是加锁的，内部还是调用WriteMessage，WriteMessage不加锁，建议用Write
				//conn.WriteMessage(websocket.TextMessage, []byte("收到消息:"+reply))             //注意: 并发对同一个连接写入会导致数据竞争
				conn.Rooms().Send(RoomName, messageType, []byte(conn.ID()+reply)) //群发
			}
		}
	}

	go func() {
		//群发
		for {
			wsServer.Rooms().Copy("test11111111111", "group2")
			log.Println("wsServer 群发消息")
			wsServer.Rooms().SendAll(websocket.TextMessage, []byte(time.Now().String()+"wsServer 群发消息"))
			time.Sleep(time.Millisecond * 1000)
		}
	}()

	http.Handle("/ws/", wsServer)

	fmt.Println("启动服务", ":12345")
	err := http.ListenAndServe(":12345", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
