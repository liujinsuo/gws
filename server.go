package gws

import (
	"context"
	"gitee.com/liujinsuo/tool"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"sync"
)

type WebsocketServer struct {
	//x包的websocket
	//*websocket.Conn

	//所有连接
	rooms Rooms

	//Handshake(握手) 是WebSocket握手中的可选功能。 例如，您可以检查或不检查原始标头。 在另一个示例中，您可以选择config.Protocol。
	//Handshake func(*websocket.Config, *http.Request) error

	//处理WebSocket连接。
	Handler func(*Conn)

	Upgrader websocket.Upgrader

	ResponseHeader http.Header
}

func NewWebsocketServer() *WebsocketServer {
	s := &WebsocketServer{}
	s.rooms = newRooms()
	return s
}

// Rooms 获取房间集合对象
func (s *WebsocketServer) Rooms() Rooms {
	return s.rooms
}

func (s *WebsocketServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := s.Upgrader.Upgrade(w, r, s.ResponseHeader)
	if err != nil {
		log.Println(err)
		return
	}
	//uuidV := uuid.NewV1().String()
	uuidV := tool.NewMongoID().ToBase62()
	ctx, cancelFunc := context.WithCancel(context.Background())
	newConn := &Conn{
		Conn:          conn,
		request:       r,
		wio:           sync.Mutex{},
		id:            uuidV,      //唯一id
		Context:       ctx,        //上下文数据
		contextCancel: cancelFunc, //
		rooms:         s.rooms,    //群发相关接口
	}
	if s.Handler != nil {
		s.Handler(newConn)
	}
}
