package gws

import (
	"sync"
)

type Rooms interface {
	Join(RoomName string, conn *Conn)                                //加入房间
	Leave(RoomName string, connID string)                            //离开房间
	LeaveAll(connID string)                                          //离开所有房间
	Clear(RoomName string)                                           //清空房间
	Send(RoomName string, messageType int, msg []byte) (err error)   //将给定事件和参数发送到指定房间中的所有连接
	SendAll(messageType int, msg []byte) (err error)                 //将给定事件和参数发送到所有房间的所有连接
	ForEach(RoomName string, f func(connID string, conn *Conn) bool) //遍历roomName房间中的所有连接
	Len(RoomName string) int                                         //给出房间中的连接数
	Rooms(conn *Conn) []string                                       //获取conn加入的所有房间
	AllRooms() []string                                              //提供了所有可供广播的房间列表
	Copy(DstRoomName string, SrcRoomName string)                     //将 SrcRoomName 房间的所有链接复制到 DstRoomName 房间一份
	GetConn(RoomName string) []*Conn                                 //获取房间中的所有连接
	CloseAllConn()                                                   //关闭全服所有连接
}

// 房间集合
type roomsT struct {
	data  map[string]*roomT
	mutex sync.RWMutex
}

func newRooms() *roomsT {
	return &roomsT{
		data:  make(map[string]*roomT),
		mutex: sync.RWMutex{},
	}
}

// MustRoom 必须获取一个房间
// 如果房间不存在就创建一个房间
func (s *roomsT) MustRoom(RoomName string) *roomT {
	if r, ok := s.Get(RoomName); ok == true { //判断房间是否存在
		return r
	} else {
		r = newRoom()
		s.Set(RoomName, r) //添加房间
		return r
	}
}

// Get 获取一个房间
func (s *roomsT) Get(RoomName string) (*roomT, bool) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	if _, ok := s.data[RoomName]; ok == true {
		return s.data[RoomName], ok
	} else {
		return nil, ok
	}
}

// Set 创建一个房间，如果存在同名的房间则会覆盖
func (s *roomsT) Set(RoomName string, room *roomT) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.data[RoomName] = room
}

// Range 遍历所有房间
func (s *roomsT) Range(f func(RoomName string, room *roomT) bool) {
	if f == nil {
		return
	}
	s.mutex.RLock()
	for k, v := range s.data {
		s.mutex.RUnlock()
		f(k, v) //这里由使用方控制有可能执行时间很长
		s.mutex.RLock()
	}
	s.mutex.RUnlock()
}

// Delete 删除房间
func (s *roomsT) Delete(RoomName string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	delete(s.data, RoomName)
}

// DeleteEmpty 删除空房间
func (s *roomsT) DeleteEmpty(RoomName string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	if s.data[RoomName] != nil && s.data[RoomName].Length() <= 0 {
		delete(s.data, RoomName)
	}
}

func (s *roomsT) Length() int {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return len(s.data)
}

// Join 加入房间
func (s *roomsT) Join(RoomName string, conn *Conn) {
	s.MustRoom(RoomName).Set(conn.ID(), conn)
}

// Leave 离开房间
func (s *roomsT) Leave(RoomName string, connID string) {
	room, ok := s.Get(RoomName)
	if ok == false {
		return //房间不存在什么也不做
	}
	room.Delete(connID)
	s.DeleteEmpty(RoomName) //如果房间为空则删除房间
}

// LeaveAll 离开所有房间
func (s *roomsT) LeaveAll(connID string) {
	s.Range(func(roomName string, room *roomT) bool {
		room.Delete(connID)
		s.DeleteEmpty(roomName) //如果房间为空则删除房间
		return true
	})
}

// Clear 清空房间
func (s *roomsT) Clear(roomName string) {
	s.Delete(roomName)
}

// Send 将给定事件和参数发送到指定房间中的所有连接
func (s *roomsT) Send(RoomName string, messageType int, msg []byte) (err error) {
	room, ok := s.Get(RoomName)
	if ok == false {
		return nil //房间不存在什么也不做
	}
	room.Range(func(connID string, conn *Conn) bool {
		if err = conn.Write(messageType, msg); err != nil {
			//todo 这不能反false
			//return false
		}
		return true
	})
	return err
}

// SendAll  将给定事件和参数发送到所有房间的所有连接
func (s *roomsT) SendAll(messageType int, msg []byte) (err error) {
	s.Range(func(RoomName string, room *roomT) bool {
		room.Range(func(connID string, conn *Conn) bool {
			if err = conn.Write(messageType, msg); err != nil {
				//todo 这不能反false
				//return false
			}
			return true
		})
		return true
	})
	return err
}

// ForEach 遍历roomName房间中的所有连接
func (s *roomsT) ForEach(RoomName string, f func(connID string, conn *Conn) bool) {
	room, ok := s.Get(RoomName)
	if ok == false {
		return //房间不存在什么也不做
	}
	room.Range(func(connID string, conn *Conn) bool {
		if f != nil {
			f(connID, conn)
		}
		return true
	})
}

// Len 给出房间中的连接数
func (s *roomsT) Len(RoomName string) int {
	room, ok := s.Get(RoomName)
	if ok == false {
		return 0 //房间不存在什么也不做
	}
	return room.Length()
}

// Rooms 获取conn加入的所有房间
func (s *roomsT) Rooms(conn *Conn) []string {
	rt := make([]string, s.Length())
	s.Range(func(roomName string, room *roomT) bool {
		if _, ok := room.Get(conn.ID()); ok == true {
			rt = append(rt, roomName)
		}
		return true
	})
	return rt
}

// AllRooms 提供了所有可供广播的房间列表
func (s *roomsT) AllRooms() []string {
	rt := make([]string, s.Length())
	s.Range(func(roomName string, room *roomT) bool {
		rt = append(rt, roomName)
		return true
	})
	return rt
}

// Copy 将SrcRoomName 房间的所有链接复制到 DstRoomName 房间一份
func (s *roomsT) Copy(DstRoomName string, SrcRoomName string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	src, ok := s.data[SrcRoomName]
	if ok == false {
		return
	}
	dst, ok := s.data[DstRoomName]
	if ok == false {
		dst = newRoom() //创建目标房间
		s.data[DstRoomName] = dst
	}
	src.Range(func(connID string, conn *Conn) bool {
		dst.Set(connID, conn)
		return true
	})
}

// GetConn 获取房间中的所有连接
func (s *roomsT) GetConn(RoomName string) []*Conn {
	room, ok := s.Get(RoomName)
	if ok == false {
		return nil
	}
	var rt = make([]*Conn, 0)
	room.Range(func(connID string, conn *Conn) bool {
		rt = append(rt, conn)
		return true
	})
	return rt
}

// CloseAllConn 关闭整个服务的所有连接
func (s *roomsT) CloseAllConn() {
	s.Range(func(RoomName string, room *roomT) bool {
		room.CloseAllConn()
		return true
	})
}
