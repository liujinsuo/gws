package gws

import (
	"sync"
)

type roomT struct {
	connects map[string]*Conn //这里为什么不用sync.Map,原因是sync.Map打断点时看不到数据的值，不方便调试
	mutex    sync.RWMutex
}

func newRoom() *roomT {
	return &roomT{
		connects: make(map[string]*Conn),
		mutex:    sync.RWMutex{},
	}
}

func (s *roomT) Get(connID string) (conn *Conn, ok bool) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	if r, ok := s.connects[connID]; ok == true {
		return r, ok
	} else {
		return nil, ok
	}
}

func (s *roomT) Set(connID string, conn *Conn) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.connects[connID] = conn
}

func (s *roomT) Range(f func(connID string, conn *Conn) bool) {
	if f == nil {
		return
	}
	s.mutex.RLock()
	for k, v := range s.connects {
		s.mutex.RUnlock()
		f(k, v) //这里由使用方控制有可能执行时间很长,所以此函数不用defer解锁
		s.mutex.RLock()
	}
	s.mutex.RUnlock()
}

// Delete 删除房间内连接 返回剩余连接个数
func (s *roomT) Delete(connID string) int {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	delete(s.connects, connID)
	return len(s.connects)
}

func (s *roomT) Length() int {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return len(s.connects)
}

// 关闭房间内所有连接
func (s *roomT) CloseAllConn() {
	s.Range(func(connID string, conn *Conn) bool {
		_ = conn.Close()
		return true
	})
}
