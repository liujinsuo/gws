package gws

import (
	"context"
	"github.com/gorilla/websocket"
	"github.com/spf13/cast"
	"net/http"
	"sync"
	"time"
)

type Conn struct {
	*websocket.Conn                    //x包的conn
	request         *http.Request      //http请求
	wio             sync.Mutex         //写入加锁
	id              string             //当前连接的唯一id
	Context         context.Context    //用于检测连接是否关闭
	contextCancel   context.CancelFunc //标记连接为关闭状态
	data            sync.Map           //用户数据缓存
	rooms           Rooms
	closeOnce       sync.Once
}

// Close 重写Close函数
func (s *Conn) Close() (err error) {
	s.closeOnce.Do(func() {
		s.contextCancel()
		s.rooms.LeaveAll(s.id)
		err = s.Conn.Close()
	})
	return nil
}

// IsClose 判断连接是否关闭 true 已关闭 false 连接中
func (s *Conn) IsClose() (ok bool) {
	select {
	case <-s.Context.Done():
		return true
	default:
		return false
	}
}

func (s *Conn) Write(messageType int, data []byte) (err error) {
	s.wio.Lock()
	defer s.wio.Unlock()
	return s.Conn.WriteMessage(messageType, data)
}

func (s *Conn) SetWriteDeadline(t time.Time) error {
	s.wio.Lock()
	defer s.wio.Unlock()
	return s.Conn.SetWriteDeadline(t)

}

func (s *Conn) SetReadDeadline(t time.Time) error {
	s.wio.Lock()
	defer s.wio.Unlock()
	return s.Conn.SetReadDeadline(t)
}

func (s *Conn) Request() *http.Request {
	return s.request
}

// ID 获取连接id
func (s *Conn) ID() string {
	return s.id
}

// Rooms 获取房间集合对象
func (s *Conn) Rooms() Rooms {
	return s.rooms
}

// Set 缓存数据
func (s *Conn) Set(key string, value interface{}) {
	s.data.Store(key, value)
}

// Get 获取缓存的数据
func (s *Conn) Get(key string) interface{} {
	value, _ := s.data.Load(key)
	return value
}

// GetString 获取缓存的数据
func (s *Conn) GetString(key string) string {
	value, _ := s.data.Load(key)
	return cast.ToString(value)
}

// GetInt64 获取缓存的数据
func (s *Conn) GetInt64(key string) int64 {
	value, _ := s.data.Load(key)
	return cast.ToInt64(value)
}

// GetInt32 获取缓存的数据
func (s *Conn) GetInt32(key string) int32 {
	value, _ := s.data.Load(key)
	return cast.ToInt32(value)
}

// GetInt 获取缓存的数据
func (s *Conn) GetInt(key string) int {
	value, _ := s.data.Load(key)
	return cast.ToInt(value)
}

// GetUint64 获取缓存的数据
func (s *Conn) GetUint64(key string) uint64 {
	value, _ := s.data.Load(key)
	return cast.ToUint64(value)
}

// GetFloat64 获取缓存的数据
func (s *Conn) GetFloat64(key string) float64 {
	value, _ := s.data.Load(key)
	return cast.ToFloat64(value)
}

// GetBool 获取缓存的数据
func (s *Conn) GetBool(key string) bool {
	value, _ := s.data.Load(key)
	return cast.ToBool(value)
}

// GetTime 获取缓存的数据
func (s *Conn) GetTime(key string) time.Time {
	value, _ := s.data.Load(key)
	return cast.ToTime(value)
}
